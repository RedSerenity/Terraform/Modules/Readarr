data "docker_registry_image" "Readarr" {
	name = "linuxserver/readarr:nightly"
}

resource "docker_image" "Readarr" {
	name = data.docker_registry_image.Readarr.name
	pull_triggers = [data.docker_registry_image.Readarr.sha256_digest]
}

module "Readarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Readarr.latest

	networks = [{ name: var.docker_network, aliases: ["readarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 8787, external: 9605, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Readarr"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.books}"
			container_path = "/books"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}